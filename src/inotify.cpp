#include <stream9/linux/inotify/inotify.hpp>

#include <stream9/linux/inotify/namespace.hpp>
#include <stream9/linux/inotify/inotify_event_range.hpp>

#include <string>

#include <sys/ioctl.h>
#include <sys/inotify.h>
#include <unistd.h>

#include <stream9/bits.hpp>
#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/linux/read.hpp>

namespace stream9::linux {

static std::string
flags_to_symbol(int const flags)
{
    return stream9::bits::ored_flags_to_symbol(flags, {
        STREAM9_FLAG_ENTRY(IN_CLOEXEC),
        STREAM9_FLAG_ENTRY(IN_NONBLOCK),
    });
}

static std::string
mask_to_symbol(std::uint32_t const mask)
{
    return stream9::bits::ored_flags_to_symbol(mask, {
        STREAM9_FLAG_ENTRY(IN_ACCESS),
        STREAM9_FLAG_ENTRY(IN_MODIFY),
        STREAM9_FLAG_ENTRY(IN_ATTRIB),
        STREAM9_FLAG_ENTRY(IN_CLOSE_WRITE),
        STREAM9_FLAG_ENTRY(IN_CLOSE_NOWRITE),
        STREAM9_FLAG_ENTRY(IN_OPEN),
        STREAM9_FLAG_ENTRY(IN_MOVED_FROM),
        STREAM9_FLAG_ENTRY(IN_MOVED_TO),
        STREAM9_FLAG_ENTRY(IN_CREATE),
        STREAM9_FLAG_ENTRY(IN_DELETE),
        STREAM9_FLAG_ENTRY(IN_DELETE_SELF),
        STREAM9_FLAG_ENTRY(IN_UNMOUNT),
        STREAM9_FLAG_ENTRY(IN_Q_OVERFLOW),
        STREAM9_FLAG_ENTRY(IN_IGNORED),
        STREAM9_FLAG_ENTRY(IN_ISDIR),

        STREAM9_FLAG_ENTRY(IN_ONLYDIR),
        STREAM9_FLAG_ENTRY(IN_DONT_FOLLOW),
        STREAM9_FLAG_ENTRY(IN_EXCL_UNLINK),
        STREAM9_FLAG_ENTRY(IN_MASK_CREATE),
        STREAM9_FLAG_ENTRY(IN_MASK_ADD),
        STREAM9_FLAG_ENTRY(IN_ONESHOT),
    });
}

/*
 * class inotify
 */
inotify::
inotify()
    : m_fd { ::inotify_init() }
{
    try {
        if (m_fd == -1) {
            throw error {
                "inotify_init()",
                linux::make_error_code(errno),
            };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

inotify::
inotify(int const flags)
    : m_fd { ::inotify_init1(flags) }
{
    try {
        if (m_fd == -1) {
            throw error {
                "inotify_init1()",
                linux::make_error_code(errno), {
                    { "flags", flags_to_symbol(flags) },
                }
            };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

inotify_event_range inotify::
read(array_view<char> buf)
{
    try {
        auto const o_n = lx::read(m_fd, buf);
        if (o_n) {
            return array_view(buf.begin(), *o_n);
        }
        else if (o_n == EAGAIN || o_n == EINTR) {
            return array_view(buf.begin(), 0);
        }
        else {
            throw error {
                "read()",
                lx::make_error_code(o_n.error())
            };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

std::size_t inotify::
available_data_size() const
{
    try {
        int bytes = 0;

        auto const rc = ::ioctl(m_fd, FIONREAD, &bytes);
        if (rc == -1) {
            throw error {
                "ioctl(FIONREAD)",
                linux::make_error_code(errno)
            };
        }
        assert(bytes >= 0);

        return static_cast<size_t>(bytes);
    }
    catch (...) {
        rethrow_error();
    }
}

int inotify::
add_watch(cstring_ptr const path, std::uint32_t const mask)
{
    try {
        assert(m_fd != -1);

        auto const wd = ::inotify_add_watch(m_fd, path, mask);
        if (wd == -1) {
            throw error {
                "inotify_add_watch()",
                linux::make_error_code(errno), {
                    { "fd", m_fd },
                    { "pathname", path },
                    { "mask", mask_to_symbol(mask) },
                }
            };
        }

        return wd;
    }
    catch (...) {
        rethrow_error();
    }
}

void inotify::
rm_watch(int const wd)
{
    try {
        if (::inotify_rm_watch(m_fd, wd) != 0) {
            throw error {
                "inotify_rm_watch()",
                linux::make_error_code(errno), {
                    { "fd", m_fd },
                    { "wd", wd },
                }
            };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

/*
 * class inotify_event_range
 */
void
tag_invoke(json::value_from_tag,
           json::value& v,
           inotify_event_range const& r)
{
    try {
        auto& arr = v.emplace_array();

        for (auto const& e: r) {
            arr.push_back(json::value_from(e));
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(value_from_tag, json::value& jv, struct ::inotify_event const& e)
{
    auto& obj = jv.emplace_object();
    obj["wd"] = e.wd;
    obj["mask"] = linux::mask_to_symbol(e.mask);
    if (e.cookie) {
        obj["cookie"] = e.cookie;
    }
    if (e.len) {
        obj["name"] = e.name;
    }
}

} // namespace stream9::json

