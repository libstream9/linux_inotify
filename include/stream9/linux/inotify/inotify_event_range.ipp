#ifndef STREAM9_LINUX_INOTIFY_EVENT_RANGE_IPP
#define STREAM9_LINUX_INOTIFY_EVENT_RANGE_IPP

#include "inotify_event_range.hpp"
#include "inotify_event_iterator.hpp"

namespace stream9::linux {

inline inotify_event_range::
inotify_event_range(array_view<char> const buf) noexcept
    : m_buf { buf }
{}

inline inotify_event_range::iterator inotify_event_range::
begin() const noexcept
{
    return m_buf.data();
}

inline inotify_event_range::iterator inotify_event_range::
end() const noexcept
{
    return m_buf.data() + m_buf.size();
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_INOTIFY_EVENT_RANGE_IPP
