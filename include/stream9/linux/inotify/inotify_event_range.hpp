#ifndef STREAM9_LINUX_INOTIFY_EVENT_RANGE_HPP
#define STREAM9_LINUX_INOTIFY_EVENT_RANGE_HPP

#include "namespace.hpp"

#include <stream9/array_view.hpp>
#include <stream9/ranges/range_facade.hpp>

namespace stream9::linux {

class inotify_event_iterator;

/*
 * @model std::regular
 * @model rng::forward_range
 * rng::range_reference_t == inotify_event
 */
class inotify_event_range : public rng::range_facade<inotify_event_range>
{
public:
    using iterator = inotify_event_iterator;

public:
    inotify_event_range() = default; // partially-formed

    inotify_event_range(array_view<char> buf) noexcept;

    iterator begin() const noexcept;
    iterator end() const noexcept;

private:
    array_view<char> m_buf;
};

void
tag_invoke(json::value_from_tag, json::value&, inotify_event_range const&);

} // namespace stream9::linux

#endif // STREAM9_LINUX_INOTIFY_EVENT_RANGE_HPP

#include "inotify_event_range.ipp"
