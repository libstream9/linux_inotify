#ifndef STREAM9_LINUX_INOTIFY_NAMESPACE_HPP
#define STREAM9_LINUX_INOTIFY_NAMESPACE_HPP

namespace stream9::iterators {}
namespace stream9::ranges {}
namespace stream9::json {}
namespace stream9::strings {}

namespace stream9::linux {

namespace st9 { using namespace stream9; }
namespace lx { using namespace st9::linux; }
namespace iter { using namespace st9::iterators; }
namespace json { using namespace st9::json; }
namespace rng { using namespace st9::ranges; }
namespace str { using namespace st9::strings; }

} // namespace stream9::linux

#endif // STREAM9_LINUX_INOTIFY_NAMESPACE_HPP
