#ifndef STREAM9_LINUX_INOTIFY_INOTIFY_EVENT_ITERATOR_HPP
#define STREAM9_LINUX_INOTIFY_INOTIFY_EVENT_ITERATOR_HPP

#include "namespace.hpp"

#include <sys/inotify.h>

#include <stream9/iterators.hpp>

namespace stream9::linux {

class inotify_event_iterator
    : public iter::iterator_facade<inotify_event_iterator,
                std::input_iterator_tag,
                struct ::inotify_event const& >
{
public:
    inotify_event_iterator() = default; // partially-formed

    inotify_event_iterator(char* const ptr) noexcept
        : m_ptr { ptr }
    {}

private:
    friend class iter::iterator_core_access;

    struct ::inotify_event const&
    dereference() const noexcept
    {
        return *reinterpret_cast<struct ::inotify_event*>(m_ptr);
    }

    void increment() noexcept
    {
        m_ptr += event_size();
    }

    bool equal(inotify_event_iterator const& other) const noexcept
    {
        return m_ptr == other.m_ptr;
    }

    std::size_t event_size() const noexcept
    {
        return sizeof(::inotify_event)
             + reinterpret_cast<::inotify_event*>(m_ptr)->len;
    }

private:
    char* m_ptr {};
};

} // namespace stream9::linux

#endif // STREAM9_LINUX_INOTIFY_INOTIFY_EVENT_ITERATOR_HPP
