#ifndef STREAM9_LINUX_INOTIFY_INOTIFY_HPP
#define STREAM9_LINUX_INOTIFY_INOTIFY_HPP

#include "inotify_event_range.hpp"

#include <cstdint>

#include <stream9/array_view.hpp>
#include <stream9/cstring_ptr.hpp>
#include <stream9/cstring_view.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/linux/error.hpp>

namespace stream9::linux {

/*
 * @model std::default_initializable
 * @model !std::copyable
 * @model std::movable
 */
class inotify
{
public:
    // essential
    inotify();
    inotify(int flags);

    inotify(inotify const&) = delete;
    inotify& operator=(inotify const&) = delete;
    inotify(inotify&&) = default;
    inotify& operator=(inotify&&) = default;

    ~inotify() noexcept = default;

    // accessor
    fd_ref fd() const noexcept;

    // query
    // Return rng::input_range of struct ::inotify_event&.
    // Block if ino isn't constructed with IN_NONBLOCK flag. If it is
    // specified and there is no data available, return empty range.
    inotify_event_range
    read(array_view<char> buf);

    std::size_t available_data_size() const;

    // modifier
    int add_watch(cstring_ptr path, std::uint32_t mask);
    void rm_watch(int wd);

    // converter
    operator fd_ref () const noexcept { return m_fd; }

    // equality
    bool operator==(fd_ref const& other) const noexcept { return other == m_fd; }

private:
    class fd m_fd;
};

inline cstring_view
name(struct ::inotify_event const& e)
{
    if (e.len) {
        return e.name;
    }
    else {
        return "";
    }
}

} // namespace stream9::linux

namespace stream9::json {

void
tag_invoke(value_from_tag, json::value&, struct ::inotify_event const&);

} // namespace stream9::json

#endif // STREAM9_LINUX_INOTIFY_INOTIFY_HPP

#include "inotify.ipp"
