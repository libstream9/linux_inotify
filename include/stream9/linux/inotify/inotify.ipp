#ifndef STREAM9_LINUX_INOTIFY_INOTIFY_IPP
#define STREAM9_LINUX_INOTIFY_INOTIFY_IPP

#include "inotify.hpp"

#include <stream9/linux/fd.hpp>

namespace stream9::linux {

inline fd_ref inotify::
fd() const noexcept
{
    return m_fd;
}

} // namespace stream9::linux

#endif // STREAM9_LINUX_INOTIFY_INOTIFY_IPP
